# Notification Client

- Provides the UI components to list notifications.
- WARNING: Before committing please run `yarn` so the `dist` package gets updated!

# Publish

  1. Bump the `package.json` version.
  2. Update CHANGELOG.md with changes.
  3. Merge to Master branch: the pipeline will deploy your new version in https://www.npmjs.com/package/epersonam-notification-client

__note__: during a PR, the pipeline checks if you did update both `CHANGELOG.md` and `package.json`. If not gives an error.   


# Manual Publish (old way, deprecated)

  1. Bump the `package.json` version.
  2. Update CHANGELOG.md with changes.
  3. Publish to NPM registry:
    1. Login to NPM: `npm login`.
    2. Publish to NPM: `npm publish`

## Install

Run the following, also whenever you want to update to latest version as we currently don't go with npm registry.

```
yarn add epersonam-notification-client
```

## Work locally
Node version:
v14.15.4

Build in this project with 'yarn'

Copy and paste new generated /dist/index.js  inside node_modules/ of your project