# Changelog

### 1.00.0 (Feb 7, 2025)
- up to 2 concurrent Sockets

### 0.17.0 (Nov 28, 2024)
- fix build
 
### 0.15.0 (Nov 28, 2024)
* Added emit event to send data on notification click
### 0.14.0 (Mar 12, 2024)
* removed popupBlocked call
* removed utils

### 0.13.0 (Set 26, 2022)
* modified wrong entry on package json

### 0.12.0 (Set 26, 2022)
* Added infos attribute to object emmited for digital sign
### 0.11.0 (Dic 9, 2021)


* Hide icons if don't have
* Add events for watch notifications status change
* Avoid close dropdown if have special class to clicked element
### 0.10.0 (Apr 23, 2021)

* Handle digitally signed document kind.

### 0.9.0 (Feb 01, 2021)

* Prevent notification to be auto-opened if it could be digitally signed.

### 0.8.0 (Oct 30, 2020)

* Digital Signature support

### 0.7.5 (Apr 12, 2020)

* Allow use on HR

### 0.7.3 (Mar 30, 2020)

* Fixes mail link on mobile devices.

### 0.7.1 (Oct 04, 2019)

* Fixes opacity for mail on fallback.

### 0.7.0 (Oct 03, 2019)

* Handles webSocket connections/disconnections.

### 0.6.1 (Oct 03, 2019)

* Fixes event on socket emission through eventBus.
* Adds event on open.

### 0.6.0 (Sep 20, 2019)

* Adds only-following-bus prop to mailNotifications to allow filtering of messages through business_unit_id.

### 0.5.1 (Sep 13, 2019)

* Style fix on mail head.

### 0.5.0 (Sep 13, 2019)

New feature:

* Add BU info.
* Add icons.
* Removes box-shadow and style fixes to more minimalistic layout.

### 0.4.0 (Sep 2, 2019)

New feature:

* Mail notification:
  * Adds required reading icon.
  * Adds attachment icon.
  * Reduced margin between mail notifications.

* Notification container never gets higher than 400px (overflow: scroll).

### 0.3.0 (Sep 2, 2019)

Fixes and Functionality:

* Updates lo mailNotification component to link to single message.
* Adds keep-count-notified param to allow count even of notified notifications.
* Adds mainNotification notification type.
* [yarn] Upgrades some dev dependencies.
* [yarn] Upgrade axios 0.18 --> 0.19.0
* Adds some feature to notificationMenu:
  * alwaysVisible
  * custom group title
  * all url link
* [yarn] Package updates

Documentation:

- Add CAHNGELOG.md


