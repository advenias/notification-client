function isolate_gitchanged_files_v2() {
  # get diff files for this commit
  # Input $1: file extension, like rb
  # Input $2: target branch, eg: develop
  # output: list of files with your extensions which have changed 
  TARGET_BRANCH=${1:-master}
  git fetch origin "+refs/heads/${TARGET_BRANCH}:refs/remotes/origin/${TARGET_BRANCH}"
  FULL_FILE_LIST=$(git diff --name-only origin/${TARGET_BRANCH}...)
  echo "${FULL_FILE_LIST}"
}



function changelog_checker_v1() {
  # check ruby syntax of files passed by
  # isolate_gitchanged_files function
  ANYERROR=false
  TARGET_BRANCH=${1:-master}
  FILES=$(isolate_gitchanged_files_v2 ${TARGET_BRANCH})
  CHANGELOG_CHANGED=0
  PACKAGE_JSON_CHANGED=0
  
  for FILE in ${FILES}; do
    case ${FILE} in
      CHANGELOG.md)
        CHANGELOG_CHANGED=1
        echo "INFO - very good: you changed CHANGELOG.md file"
        ;;
      package.json)
        PACKAGE_JSON_CHANGED=1
        echo "INFO - very good: you changed package.json file"
        ;;
    esac
  done
  
  if [[ $CHANGELOG_CHANGED -eq 1 ]] && [[ $PACKAGE_JSON_CHANGED -eq 1 ]]; then
    echo "INFO - Good boy, you changed both package.json and CHANGELOG.md"
  else
    echo "ERROR - Nasty guy: step back and update CHANGELOG.md and/or package.json!!!"
    if [[ ${CHANGELOG_PACKAGEJSON_CHECKER} != "true" ]]; then
      echo "INFO - but you chose to not to fail because of CHANGELOG_PACKAGEJSON_CHECKER repository var"
    else 
      exit 1
    fi
  fi
}