import { Socket } from 'phoenix'
import { eventBus } from './eventBus'
import configuration from './configuration'

export const webSocketService = {
  instance () {
    return this.socket || this.buildInstance()
  },

  buildInstance () {
    console.log(window.config.notificationService.notifSocketHost);
    this.socket = new Socket(window.config.notificationService.notifSocketHost, {
      params: {
        token: configuration.getJwt(),
        user_infos: window.config.user,
      },
    })
    this.socket.onError((e) => eventBus.$emit('webSocketService.error', e))
    this.socket.onClose((e) => eventBus.$emit('webSocketService.close', e))
    this.socket.onOpen((e) => eventBus.$emit('webSocketService.open', e))
    this.socket.connect()
    return this.socket
  },

}

export const webSocketConcurrency = {
  instance () {
    return this.concurrentSocket || this.buildInstance()
  },
  buildInstance () {
    console.log(window.config.notificationService.concurrentSocketHost);
    this.concurrentSocket = new Socket(window.config.notificationService.concurrentSocketHost, {
      params: {
        token: configuration.getJwt(),
        user_infos: window.config.user,
      },
    })
    this.concurrentSocket.onError((e) => eventBus.$emit('webSocketService.councurrency.error', e))
    this.concurrentSocket.onClose((e) => eventBus.$emit('webSocketService.councurrency.close', e))
    this.concurrentSocket.onOpen((e) => eventBus.$emit('webSocketService.councurrency.open', e))
    this.concurrentSocket.connect()
    return this.concurrentSocket
  },
}
